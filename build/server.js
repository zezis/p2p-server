"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dgram = require("dgram");
var server = dgram.createSocket("udp4");
server.on("listening", () => {
    console.log("listening...");
});
server.on("message", (msg, remote) => {
    console.log(remote.address + " : " + remote.port + ' - ' + msg);
});
console.log("P2P Server");
server.bind(40000, "127.0.0.1");
console.log("Binded");
